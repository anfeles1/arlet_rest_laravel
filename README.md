# ARLET_REST_LARAVEL


## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/anfeles1/arlet_rest_laravel.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/anfeles1/arlet_rest_laravel/-/settings/integrations)


## Name
ARLET

## Description
Proyecto de ejemplo de API REST para ARLET(Aplicativo Registro de Llegadas Tarde) con backend en Laravel y a futuro con frontend con Laravel y plugin de client api rest.

## Visuals
Muy pronto

## Authors and acknowledgment
anfelesvillada@gmail.com

## License
For open source projects, say how it is licensed.

## Project status
En Desarrollo
